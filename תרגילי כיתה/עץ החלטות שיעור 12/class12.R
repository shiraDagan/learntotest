setwd("C:/Users/liorab/Desktop/����� �����/����� 12")
shopping.raw <- read.csv("online_shopping.csv")
str(shopping.raw)
#������ ��������� ���� ��� revenue
#revenue �� �� ����� ����� ��� �� ��

summary(shopping.raw)

#����� ����� ���� ����� ������ �� ����� ���� ���� ��

library(ggplot2)
ggplot(shopping.raw, aes(Administrative, fill=Revenue))+geom_bar(position='fill')
#administrative �� ����� ������ ���� ������� ��� �����

ggplot(shopping.raw, aes(Informational, fill=Revenue))+geom_bar(position='fill')
#������ ��� ����� ������ ����� �������������� ���
#���� ����� ���� 15 ����� �� ��� �� �������

#����� ����� ������� �������
ggplot(shopping.raw, aes(ProductRelated, fill=Revenue))+geom_histogram()
#�� ����� ����� �� ���� �� �� ������ ���� 200 ������ ���� ���� 200
#��������� �200 �� ��� ����


ggplot(shopping.raw,aes(Revenue,ProductRelated))+geom_boxplot()
#�� ����� ����� ������� �� ������ ���� ���� ���� ������� �� ���� ������ ��� ���
#��� ����� ������� ���� ����� ����� ���� ���� ����� ��� ���
shopping <- shopping.raw

coercex <- function(x,by){
  if(x<=by) return(x)
  return(by)
}

shopping$ProductRelated <- sapply(shopping$ProductRelated,coercex, by=200)
ggplot(shopping,aes(Revenue,ProductRelated))+geom_boxplot()
ggplot(shopping, aes(ProductRelated, fill=Revenue))+geom_histogram(binwidth=50)

ggplot(shopping, aes(BounceRates, fill=Revenue))+geom_histogram()

shopping$BounceRates <- sapply(shopping$BounceRates, coercex, by=0.1)
shopping$ExitRates <- sapply(shopping$ExitRates, coercex, by=0.1)

shopping$OperatingSystems <- as.factor(shopping$OperatingSystems)
shopping$Browser <- as.factor(shopping$Browser)
shopping$TrafficType <- as.factor(shopping$TrafficType)
shopping$Region <- as.factor(shopping$Region)

#������ ����� ����� ����� ����� ����
ggplot(shopping, aes(OperatingSystems, fill=Revenue))+geom_bar(position="fill")

#build model
#����� ����� �� ���
install.packages('rpart')
library(rpart)
#����� ����� �� ���
install.packages('rpart.plot')
library(rpart.plot)

#Separate into training set and test set
library(caTools)

filter <- sample.split(shopping$Administrative, SplitRatio = 0.7)

shop.train <- subset(shopping,filter == T)
shop.test <- subset(shopping,filter == F)

#run the model
model <- rpart(Revenue~.,shop.train)
#�� ����� �� ��� ��������
#model <- rpart(Revenue~ExitRates+BounceRates,)

rpart.plot(model, box.palette = "RdBu", shadow.col="grey", nn=TRUE)
#���� �� ������ ���� ����� �� ����� ����

#YES it is for left castumer
predict.prob <- predict(model, shop.test)

prediction <- predict.prob > 0.8

actual <- shop.test$Revenue

cf <- table(prediction,actual)

#��� �������
precision <- cf['TRUE','TRUE']/(cf['TRUE','TRUE']+cf['TRUE','FALSE'])
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE']+cf['FALSE','TRUE'])

#----------------����� 13----------------------

#Random forest

install.packages('randomForest')
library(randomForest)

modelRF <- randomForest(Revenue~., data = shop.train, importance=TRUE)
#IMPORTANCE ���� ��� ������� ������ ������

predict.prob.rf <- predict(modelRF, shop.test)
#�������� ����� ������� ������ �� �� ����� �����

prediction.rf <- predict.prob.rf > 0.5

actual <- shop.test$Revenue

cf.rf <- table(prediction.rf,actual)

precision <- cf.rf['TRUE','TRUE']/(cf.rf['TRUE','TRUE']+cf.rf['TRUE','FALSE'])
recall <- cf.rf['TRUE','TRUE']/(cf.rf['TRUE','TRUE']+cf.rf['FALSE','TRUE'])

#ROC chart
install.packages('pROC')
library(pROC)

rocT <- roc(shop.test$Revenue , predict.prob, direction= ">", levels=c('TRUE','FALSE'))
rocRF <- roc(shop.test$Revenue , predict.prob.rf, direction= ">", levels=c('TRUE','FALSE'))
#�� ���� ����� ���� ����� �� ��������

plot(rocT, col='red', main='ROC chart')
par(new=TRUE)
plot(rocRF, col='blue', main='ROC chart')

#������ �� ���� ���� ���� ����� ����� �� ���� ���, 
#���� �� �auc
auc(rocT)
auc(rocRF)
#���� ����� ����� ���� ���, ���� ���� ����
