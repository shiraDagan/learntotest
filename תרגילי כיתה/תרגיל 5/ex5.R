house_data<-read.csv("price.csv")
str(house_data)

#Q2
complete.cases(house_data)
house_prepared<-house_data[complete.cases(house_data),]

#Q3
str(house_prepared)
#bedrooms.vec<-unique(house_prepared$bedrooms)
#house_prepared$bedrooms<-factor(house_prepared$bedrooms,levels=bedrooms.vec,labels=bedrooms.vec)
#str(house_prepared)
#floor.vec<-unique(house_prepared$floors)
#house_prepared$floors<-factor(house_prepared$floors,levels=floor.vec,labels=floor.vec)

house_prepared$date<-substr(house_prepared$date,1,8)

house_prepared$year<-as.integer(substr(house_prepared$date,1,4))
house_prepared$month<-as.integer(substr(house_prepared$date,5,6))
house_prepared$dateDay<-as.integer(substr(house_prepared$date,7,8))

house_prepared$date<-NULL

install.packages('ggplot2')
library(ggplot2)

#how the month affents the price
ggplot(house_prepared, aes(as.factor(month),price)) + geom_boxplot()

#how the year affents the price
ggplot(house_prepared, aes(as.factor(year),price)) + geom_boxplot()

#how the quantity of bedrooms affents the price
ggplot(house_prepared, aes(as.factor(bedrooms),price)) + geom_boxplot()

#how the square living affents the price
ggplot(house_prepared, aes(sqft_living,price)) + geom_point()+stat_smooth(method=lm)

#how the square lot affents the price
ggplot(house_prepared, aes(sqft_lot,price)) + geom_point()+stat_smooth(method=lm)

#how the quantity of floors affents the price
ggplot(house_prepared, aes(as.factor(floors),price)) + geom_boxplot()

#how view affects the price
ggplot(house_prepared, aes(as.factor(view),price)) + geom_boxplot()

#how the grade affects the price
ggplot(house_prepared, aes(as.factor(grade),price)) + geom_boxplot()

#how the condition affects the price
ggplot(house_prepared, aes(as.factor(condition),price)) + geom_boxplot()

#how the year built affects the price
ggplot(house_prepared, aes(as.factor(yr_built),price)) + geom_boxplot()

house_prepared$id<-NULL
house_prepared$zipcode<-NULL
house_prepared$lat<-NULL
house_prepared$long<-NULL
house_prepared$sqft_living15<-NULL
house_prepared$sqft_lot15<-NULL

#Q4
install.packages('caTools')
library(caTools)

filter <- sample.split(house_prepared$price, SplitRatio = 0.7)
house.train <- subset(house_prepared, filter==TRUE)
house.test<- subset(house_prepared, filter==FALSE)

model <- lm(price~.,data=house.train)

predicted.train <- predict(model,house.train)
predicted.test <- predict(model,house.test)

#Q5
MSE.train <- mean((house.train$price-predicted.train)**2)
MSE.test <- mean((house.test$price-predicted.test)**2)

MSE.train
MSE.test

MSE.train**0.5
MSE.test**0.5

mean(house_prepared$price)


