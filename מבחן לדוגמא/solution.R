#Q1

#�.	���� �� ����� ���� ����� R Studio ���� dataframe
churn.raw<-read.csv('churn.csv')
str(churn.raw)
summary(churn.raw)

churn<-churn.raw

#�.	���� �� ���� ����� ��� ������� ���� �������. ���� ����� �� ������ ���� ����� ����� �����? ����� ���� �� dataframe
churn$customerID <- NULL
#���� �� ���� ������ V
View(churn)

#�.	���� ����� ���� ����� ������? ���� ���� ������
#���� �� ���� ������� ��� ���
table(churn.raw$SeniorCitizen)
#Make SeniorCitizen a factor 
churn$SeniorCitizen<- as.factor(churn$SeniorCitizen)
str(churn)


#Question 2
install.packages('ggplot2')
library(ggplot2)
#how tenure affect churn
#ggplot(churn,aes(Churn,tenure)) + geom_boxplot()
ggplot(churn, aes(tenure, fill = Churn)) +geom_bar()
ggplot(churn, aes(tenure, fill = Churn)) +geom_bar(position = "fill")



#������ �� ������ ���� �� ������� ��� ����� �� ���� 0
filter <- churn$tenure == 0
churn <- churn[!filter,]

#does gender affect churn?
ggplot(churn, aes(gender , fill = Churn)) +geom_bar()
#���� ���� ����� ��� ���� ������ ������

#does contract affect churn? 
ggplot(churn, aes(Contract , fill = Churn)) +geom_bar()
ggplot(churn, aes(Contract, fill = Churn)) +geom_bar(position = "fill")
#���� ��� ��� ���� �����  ������ �����
#���� ������ ��� ���� ������ ������ ���� ���� ��� ����� ������ ���� ��� �� ������
#A big impact Nonth-to-Month customers tend to churn much more than one year
#And two year customers 

# PaperlessBilling ������ ����� ������ ����� ������. 
confusion.matrix <- table(churn$PaperlessBilling , churn$Churn)

precision  <- confusion.matrix['Yes','Yes']/(confusion.matrix['Yes','Yes'] + confusion.matrix['Yes', 'No'])
recall <- confusion.matrix['Yes','Yes']/(confusion.matrix['Yes','Yes'] + confusion.matrix['No','Yes'])
#> precision
#[1] 0.3358925
#���� ������ ������ ������, ���� �34%
#> recall
#[1] 0.7490637
#���� ����� ������ ������ �����, ����� ���� � 34%

#������ ���� ���� ������ ����� ��� ����

#prediction.spam <- prediction[,'spam']
#predicted <- prediction.spam > 0.5
#actual <- spam.df.test$type
#confusion.matrix <- table(predicted, actual)


#Q3
#divide into trainig set and test set 
library(caTools)

#����� �data
#���� ��� �� ����� ����� ���� �� ��� partner 
filter <-sample.split(churn$Churn, SplitRatio = 0.7)

churn.train <- subset(churn, filter == TRUE)
churn.test<- subset(churn, filter == FALSE)

dim(churn.train)
dim(churn.test)

#����� �����:
#  �� ���� �� ���� ��� ������ ��� ���� ����, ���� ���:
install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)

#���� �� ������
  model <- rpart(Churn~Contract+tenure+TechSupport,churn.train)
  rpart.plot(model,box.palette = 'RdBu', shadow.col = "grey", nn = TRUE)
  
  #calculating the confusion matrix
  predict.prob <- predict(model,churn.test)
  predict.prob.yes <- predict.prob[,'Yes']
  prediciton <- predict.prob.yes > 0.5
  actual <- churn.test$Churn
  
  
  confusion.matrix <- table(prediciton,actual)
 
  precision  <- confusion.matrix['TRUE','Yes']/(confusion.matrix['TRUE','Yes'] + confusion.matrix['TRUE', 'No'])
  recall <- confusion.matrix['TRUE','Yes']/(confusion.matrix['TRUE','Yes'] + confusion.matrix['FALSE','No'])
  #> precision
  #[1] 0.6300366
  # > recall
  # [1] 0.1061728
  
  
  
  
  total_errors <- (confusion.matrix['TRUE','No']+confusion.matrix['FALSE','Yes'])/dim(churn.test)[1]
  
  #�� �� ��� �����, ����� ����� �� ����� �����
  #computing base level 
  #����� ���� ����� ������ �����
  number.churn <- dim(churn[churn$Churn=='Yes',])[1]
  base_level <- number.churn/dim(churn)[1]
  #�� ���� ����� ���� ���� �������
  #��� 50 ���� ���� ������ ����� �50 ���� ���� ��� �����
  #Model improved errors from 27% in base level to 21% with model 

#Q4
  install.packages('e1071')
  library(e1071)
  
  #���� ����� ����
  #����� �� ������� ��������, ����� ������
  modelNB <- naiveBayes(Churn~.-TotalCharges-MonthlyCharges-tenure,churn.train)
  prediction <- predict(modelNB, churn.test,type = 'raw')
  prediction_chur <- prediction[,'Yes']
  actual <- churn.test$Churn 
  
  predicted <- prediction_chur > 0.5
  
  #confusion matrix
  cfNB <- table(predicted,actual)
  
  #             actual
  #predicted   No  Yes
  #FALSE      1070  105
  #TRUE        482  456
  #������- �482 ������� ��� ���������� ��� �� ���� ���� �����
  #� 456 ������� ��� ���� ������ ����� ������ ����� ������
  #��� ����
  
  precision <- cfNB['TRUE','Yes']/( cfNB['TRUE','Yes']+ cfNB['TRUE','No']) 
  #> precision
  #[1] 0.4665992
  #���� 70% ������� ����� ������ ���� ����
  recall <- cfNB['TRUE','Yes']/( cfNB['TRUE','Yes']+ cfNB['FALSE','Yes'])   
#> recall
 # [1] 0.8217469
#���� 12% ������� ����� ����� ����� ��� ��� �����
  
  #�.	������� ROC curve, ���� ��� �������. �� ������? 
  #ROC chart
  #������ ��� 2 ������ �� ���� ���
  install.packages('pROC')
  library(pROC)
  
  #ROCT ��� ��
  #ROCRF ��� �� �����- ���� ����
  rocCurveTR <- roc(churn.test$Chur , predict.prob.yes, direction= ">", levels=c("No","Yes"))
  rocCurveNB <- roc(churn.test$Chur ,  prediction_chur, direction= ">", levels=c("No","Yes"))
  #�� ���� ����� ���� ����� �� ��������
  
  #����� ��� ���� ���� ����
  plot(rocCurveTR, col='red', main='ROC chart')
  par(new=TRUE)#����� 2 ����� ����� ��
  plot(rocCurveNB, col='blue', main='ROC chart')
  #����� �� ���� ���� ������. ��� ����� ���� ���� ���� ���
  
  #AUC ���� ����� ������ ���� ���� ��� ���� ������� ���� ��� ���� ����
  #������ �� ���� ���� ���� ����� ����� �� ���� ���, 
  #���� �� �auc
##Calculate AUC
  auc(rocCurveTR)
  auc(rocCurveNB)
  #���� �� ������ ���� ���
  
  #�.	�� ���� ������� ������� ����� ����� ���� ��� ����� ����. ���� �������� ����? 
  #���� ���� �� ������ ������ ������ �� ������ ���� ���� �������� ����� ���� ������ ����� �� ������ ����� ���� ������.
                          