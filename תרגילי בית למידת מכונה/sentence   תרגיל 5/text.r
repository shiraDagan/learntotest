sales.raw<-read.csv('sales.csv', stringsAsFactors = FALSE)
stopwordlist <- read.csv("words.csv")

str(sales.raw)
head(sales.raw)

install.packages('tm')
library(tm)

sales<-sales.raw
sales$category <- as.factor(sales$category)

install.packages('ggplot2')
library(ggplot2)

ggplot(sales, aes(category)) +geom_bar()
sales_corpus <- Corpus(VectorSource(sales$text))

sales_corpus[[1]][[1]]
sales_corpus[[1]][[2]]

#remove punctuation
clean_corpus <- tm_map(sales_corpus,removePunctuation)
clean_corpus[[1]][[1]]

#remove digits
clean_corpus <- tm_map(clean_corpus,removeNumbers)
clean_corpus[[5]][[1]]#������� ����� ������� �����

#remove stop words
#����� ���� ������ ���� ������ �� ���� ��� ��� ���� ����� ������ ����
#������ ������� ������
clean_corpus <- tm_map(clean_corpus, removeWords, stopwordlist$word)
#������ ������ ���� ����
clean_corpus[[12]][[1]]#������ ��� ����� �����
clean_corpus[[3]][[1]]

#  Multiple whitespace characters are collapsed to a single blank
clean_corpus <- tm_map(clean_corpus,stripWhitespace)
clean_corpus[[3]][[1]]#������� �������


#generate the document matrix
dtm <- DocumentTermMatrix(clean_corpus)
dim(dtm)

#removing infrequent terms (that do not apear at least 10 times)
frequent_dtm <-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,10)))
dim(frequent_dtm )

install.packages('wordcloud')
library(wordcloud)

# color pallet for the word cloud
pal <- brewer.pal(9,'Dark2')

install.packages("SnowballC")
library(SnowballC)

clean_corpus <- tm_map(clean_corpus,stemDocument)
wordcloud(clean_corpus[sales$category == 'sales'], min.freq = 5, random.order = TRUE, colors = pal)
wordcloud(clean_corpus[sales$category == 'support'], min.freq = 5, random.order = TRUE, colors = pal)

#replace numbers with 'yes' or 'no' 

convert_yesno <- function(x){
  if(x==0)
  return ('no')
  return ('yes')
}

yesno_matrix <- apply(frequent_dtm, MARGIN = 1:2, convert_yesno)
yesno_matrix

sales.df <- as.data.frame(yesno_matrix)
str(sales.df)

sales.df$category<-sales$category
str(sales.df)
dim(sales.df)

#Spliting to training and testing data sets
randomvector <- runif(1181)
filter <- randomvector > 0.3
#����� ���� ����� ������ ���
sales.df.train <- sales.df[filter,]
sales.df.test <- sales.df[!filter,]
#����� �������
dim(sales.df.train)
dim(sales.df.test)

#run naive base algorithm to generate a model
install.packages('e1071')
library(e1071)

model <-naiveBayes(sales.df.train[,-268],sales.df.train$category)
model

#����� ����� �������� �� �������
#prediction on the test set
prediction <- predict(model, sales.df.test[,-268],type = 'raw')
prediction_sales<-prediction[,'support']
actual <- sales.df.test$category
predicted <- prediction_sales > 0.5

#confusion matrix
conf_matrix <- table(predicted,actual)

precision<-conf.matrix[2,2]/(conf.matrix[2,2]+conf.matrix[2,1])
precision
recall<-conf.matrix[2,2]/(conf.matrix[2,2]+conf.matrix[1,2])
recall


